package main

import (
	"net"
	"post-service/config"
	ps "post-service/genproto/post"
	"post-service/kafka/consumer"
	"post-service/pkg/db"
	"post-service/pkg/logger"
	"post-service/service"
	grpcclient "post-service/service/grpcClient"
	"sync"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "golang")
	defer logger.CleanUp(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)

	connDB, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error", logger.Error(err))
	}
	client, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Error while connecting to clients", logger.Error(err))
	}
	// kafka ui
	kafCons, closefunc, err := consumer.NewPostconsumer(cfg, log, connDB)
	if err != nil {
		log.Fatal("Error while connecting to kafka")
	}
	defer closefunc()

	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		kafCons.Post().ProducerCreateConsumerCode()
		wg.Done()
	}()
	wg.Wait()

	postService := service.NewPostService(connDB, log, client)
	listen, err := net.Listen("tcp", cfg.RPCHost+":"+cfg.RPCPort)

	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	ps.RegisterPostServiceServer(s, postService)
	log.Info("main:server running",
		logger.String("port", cfg.RPCPort),
	)
	if err := s.Serve(listen); err != nil {
		log.Fatal("error while listening2: %v", logger.Error(err))
	}
}
