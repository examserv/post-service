package post

import (
	"context"
	"encoding/json"
	"fmt"
	"post-service/config"
	pc "post-service/genproto/customer"
	ps "post-service/genproto/post"
	"post-service/pkg/logger"
	"post-service/storage"

	"github.com/jmoiron/sqlx"
	"github.com/segmentio/kafka-go"
)

type ProducerCreateConsumer struct {
	Reader    *kafka.Reader
	ConnClose func()
	Cfg       config.Config
	Logger    logger.Logger
	Storage   storage.IStorage
}

func NewProducerCreateconsumer(cfg config.Config, db *sqlx.DB) (*ProducerCreateConsumer, error) {
	r := kafka.NewReader(
		kafka.ReaderConfig{
			Brokers:   []string{"kafka:29092"},
			Topic:     cfg.ConsumerTopic,
			Partition: 0,
			MinBytes:  10e3,
			MaxBytes:  10e6,
		},
	)
	return &ProducerCreateConsumer{
		Reader:  r,
		Storage: storage.NewStoragePg(db),
		ConnClose: func() {
			r.Close()
		},
		Cfg: cfg,
	}, nil
}

func (p *ProducerCreateConsumer) ProducerCreateConsumerCode() {
	for {
		msg, err := p.Reader.ReadMessage(context.Background())
		if err != nil {
			fmt.Println(err)
			return
		}
		req := &pc.CustomerReq{}
		err = json.Unmarshal(msg.Value, req)
		if err != nil {
			fmt.Println("error while unmarsheling in kafka message", err)
			return
		}
		for _, pst := range req.Posts {
			mod := &ps.PostReq{
				UserId:      pst.UserId,
				Description: pst.Description,
				Name:        pst.Name,
			}
			p.Storage.Post().CreatePost(mod)

		}
		// 1. Parse to struct
		// 2. Give it to crete post method
		fmt.Println("Message is recieved: ", string(msg.Value))
	}
}
