CURRENT_DIR=$(shell pwd)
APP=template
APP_CMD_DIR=./cmd

build:
	CGO_ENABLED=0 GOOS=darwin go build -mod=vendor -a -installsuffix cgo -o ${CURRENT_DIR}/bin/${APP} ${APP_CMD_DIR}/main.go

proto-gen:
	./scripts/gen-proto.sh	${CURRENT_DIR}

lint: ## Run golangci-lint with printing to stdout
	golangci-lint -c .golangci.yaml run --build-tags "musl" ./...
# @database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com
migrate_up:
	migrate -source file://migrations/ -database postgres://jamshidbek:12345@database-1.c9lxq3r1itbt.us-east-1.rds.amazonaws.com:5432/postdb_jamshidbek up

submod_up:
	git submodule update --remote --merge

run:
	go run cmd/main.go