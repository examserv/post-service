CREATE TABLE IF NOT EXISTS posts(
    id serial primary key,
    name text not null,
    description text,
    user_id uuid NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);