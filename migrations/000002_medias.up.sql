CREATE TABLE IF NOT EXISTS medias(
    id serial primary key,
    name text not null,
    link text,
    type text,
    post_id BIGINT REFERENCES posts(id),
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);
