package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment      string
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel         string
	RPCPort          string
	RPCHost          string
	KafkaHost        string
	ConsumerTopic    string

	ReviewServiceHost string
	ReviewServicePort int
}

func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))
	// mydatabase.cqwczror7wp5.us-east-1.rds.amazonaws.com
	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "postdb"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "postgres"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "123"))
	c.ReviewServiceHost = cast.ToString(GetOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToInt(GetOrReturnDefault("REVIEW_SERVICE_PORT", 3333))
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))
	c.RPCPort = cast.ToString(GetOrReturnDefault("RPC_PORT", "2222"))
	c.RPCHost = cast.ToString(GetOrReturnDefault("RPC_HOST", "localhost"))

	c.KafkaHost = cast.ToString(GetOrReturnDefault("KAFKA_HOST", "kafka:9092"))
	c.ConsumerTopic = cast.ToString(GetOrReturnDefault("CONSUMER_TOPIC", "postTopic"))
	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
