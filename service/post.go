package service

import (
	"context"
	"fmt"

	pb "post-service/genproto/post"
	"post-service/genproto/review"
	"post-service/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (p *PostService) CreatePost(ctx context.Context, req *pb.PostReq) (*pb.PostResp, error) {
	res, err := p.Storage.Post().CreatePost(req)
	if err != nil {
		p.Logger.Error("Error while creating post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't create your post, try again")
	}
	return res, nil
}

func (p *PostService) UpdatePost(ctx context.Context, req *pb.PostReq) (*pb.PostReq, error) {
	res, err := p.Storage.Post().UpdatePost(req)
	if err != nil {
		p.Logger.Error("Error while Updating post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't update your post, try again")
	}
	return res, nil
}

func (p *PostService) DeletePostId(ctx context.Context, req *pb.PostId) (*pb.Empty, error) {
	_, err := p.Storage.Post().DeletePostID(req)
	if err != nil {
		p.Logger.Error("Error while Deleting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	_, err = p.Client.Review().DeleteReviewPostId(ctx, &review.ReviewPostId{PostId: req.Id})
	if err != nil {
		fmt.Println(err)
		p.Logger.Error("Error while Deleting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	return &pb.Empty{}, nil
}

func (p *PostService) DeletePostUserID(ctx context.Context, req *pb.PostUserId) (*pb.Empty, error) {
	_, err := p.Storage.Post().DeletePostUserID(&pb.PostUserId{UserId: req.UserId})
	if err != nil {
		p.Logger.Error("Error while Deleting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	_, err = p.Client.Review().DeleteReviewUserId(ctx, &review.ReviewUserId{UserId: req.UserId})
	if err != nil {
		fmt.Println(err)
		p.Logger.Error("Error while Deleting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't delete your post")
	}
	return &pb.Empty{}, nil
}

func (p *PostService) GetPostId(ctx context.Context, req *pb.PostId) (*pb.PostResp, error) {
	res, err := p.Storage.Post().GetPostId(req)
	if err != nil {
		p.Logger.Error("Error while getting post", logger.Error(err))
		return nil, status.Error(codes.Internal, "Couldn't get the post")
	}
	response := &pb.PostResp{
		Id:          res.Id,
		Name:        res.Name,
		Description: res.Description,
		UserId:      res.UserId,
		Medias:      res.Medias,
	}
	reviews, err := p.Client.Review().GetRewPostId(ctx, &review.ReviewPostId{PostId: response.Id})
	if err != nil {
		p.Logger.Error("Error while getting post reviews", logger.Error(err))
	}
	for _, rew := range reviews.Reviews {
		response.Reviews = append(response.Reviews, &pb.Review{
			Id:          res.Id,
			PostId:      req.Id,
			UserId:      rew.UserId,
			Description: rew.Description,
			Rating:      rew.Rating,
		})
	}
	return response, nil
}

func (p *PostService) GetPostUserId(ctx context.Context, req *pb.PostUserId) (*pb.Posts, error) {
	// fmt.Print("hello world")
	res, err := p.Storage.Post().GetPostUserId(req)
	if err != nil {
		fmt.Println(err)
		p.Logger.Error("Error while getting posts", logger.Error(err))
		return &pb.Posts{}, status.Error(codes.Internal, "Couldn't get the posts")
	}
	response := &pb.Posts{}
	for _, rsp := range res.Posts {
		post := &pb.PostResp{
			Id:          rsp.Id,
			Name:        rsp.Name,
			Description: rsp.Description,
			UserId:      rsp.UserId,
			Medias:      rsp.Medias,
		}
		revs, err := p.Client.Review().GetRewPostId(ctx, &review.ReviewPostId{PostId: post.Id})
		// fmt.Println(revs)
		if err != nil {
			post.Reviews = []*pb.Review{}
			response.Posts = []*pb.PostResp{}
			// return &pb.Posts{}, err
			post.Reviews = []*pb.Review{}
		}
		for _, rev := range revs.Reviews {
			post.Reviews = append(post.Reviews, &pb.Review{
				Id:          rev.Id,
				Name:        rev.Name,
				PostId:      rev.PostId,
				UserId:      rev.UserId,
				Description: rev.Description,
				Rating:      rev.Rating,
			})
		}
		response.Posts = append(response.Posts, post)
	}
	return response, nil
}
