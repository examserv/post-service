package grpcclient

import (
	"fmt"

	"post-service/config"
	rs "post-service/genproto/review"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Cleints interface {
	Review() rs.ReviewServiceClient
}
type ServiceManager struct {
	config        config.Config
	reviewService rs.ReviewServiceClient
}

func New(c config.Config) (Cleints, error) {
	review, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.ReviewServiceHost, c.ReviewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return &ServiceManager{}, nil
	}
	return &ServiceManager{
		config:        c,
		reviewService: rs.NewReviewServiceClient(review),
	}, nil
}

func (s *ServiceManager) Review() rs.ReviewServiceClient {
	return s.reviewService
}
