package service

import (
	"post-service/pkg/logger"
	grpcclient "post-service/service/grpcClient"
	"post-service/storage"

	"github.com/jmoiron/sqlx"
)

type PostService struct {
	Storage storage.IStorage
	Logger  logger.Logger
	Client  grpcclient.Cleints
}

func NewPostService(db *sqlx.DB, l logger.Logger, client grpcclient.Cleints) *PostService {
	return &PostService{
		Storage: storage.NewStoragePg(db),
		Logger:  l,
		Client:  client,
	}
}
