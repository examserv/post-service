package storage

import (
	"post-service/storage/postgres"
	"post-service/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Post() repo.PostStoreI
}

type StoragePg struct {
	Db       *sqlx.DB
	customerRepo repo.PostStoreI
}

func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:       db,
		customerRepo: postgres.NewPostRepo(db),
	}
}

func (s *StoragePg) Post() repo.PostStoreI {
	return s.customerRepo
}
