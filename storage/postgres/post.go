package postgres

import (
	"fmt"
	ps "post-service/genproto/post"

	"github.com/jmoiron/sqlx"
)

type PostRepo struct {
	Db *sqlx.DB
}

func NewPostRepo(db *sqlx.DB) *PostRepo {
	return &PostRepo{Db: db}
}

func (p *PostRepo) CreatePost(res *ps.PostReq) (*ps.PostResp, error) {
	// fmt.Println(res)
	trs, err := p.Db.Begin()
	if err != nil {
		fmt.Println("error while inserting new customer by transaction")
		return &ps.PostResp{}, err
	}
	defer trs.Rollback()
	responsePost := ps.PostResp{}
	err = trs.QueryRow(`insert into posts(
		user_id,
		name,
		description
		) 
		values($1, $2, $3) 
		returning id, 
		user_id, 
		name,
		description`,
		res.UserId,
		res.Name,
		res.Description).Scan(
		&responsePost.Id,
		&responsePost.UserId,
		&responsePost.Name,
		&responsePost.Description)
	if err != nil {
		fmt.Println("error while inserting posts")
		return &ps.PostResp{}, err
	}

	for _, media := range res.Medias {
		mediaResp := ps.Media{}
		err := trs.QueryRow(`insert into medias(post_id,name,link,type)
			values ($1,$2,$3,$4)`,
			responsePost.Id, media.Name, media.Link, media.Type).Scan(
			&mediaResp.PostId,
			&mediaResp.Name,
			&mediaResp.Link,
			&mediaResp.Type,
		)
		if err != nil {
			fmt.Println("error while inserting media", err)
		}
		responsePost.Medias = append(responsePost.Medias, &mediaResp)
	}
	if err = trs.Commit(); err != nil {
		fmt.Println(err)
	}
	return &responsePost, nil
}
func (p *PostRepo) GetPostId(req *ps.PostId) (*ps.PostResp, error) {
	response := &ps.PostResp{}
	err := p.Db.QueryRow(`SELECT 
		id,
		user_id, 
		name,
		description
		FROM posts
		where id=$1 and deleted_at is null
	`, req.Id).Scan(
		&response.Id,
		&response.UserId,
		&response.Name,
		&response.Description,
	)
	if err != nil {
		return nil, err
	}
	response.Medias, err = p.GetMediasByPostId(&ps.PostId{
		Id: response.Id,
	})
	if err != nil {
		return nil, err
	}
	return response, nil
}
func (p *PostRepo) UpdatePost(res *ps.PostReq) (*ps.PostReq, error) {
	_, err := p.Db.Exec(`update posts set name=$1, description=$2, user_id=$3, updated_at=now() where id=$4 returning id`,
		res.Name, res.Description, res.UserId, res.Id)
	if err != nil {
		return &ps.PostReq{}, err
	}		
	err = p.UpdateMediapostId(res.Medias)
	if err != nil {
		return &ps.PostReq{}, err
	}
	return res, err
}

func (p *PostRepo) DeletePostID(req *ps.PostId) (*ps.Empty, error) {
	err := p.DeleteMedia(req)
	if err != nil {
		fmt.Println("error while deleting media")
		return &ps.Empty{}, err
	}
	_, err = p.Db.Exec(`UPDATE posts SET deleted_at=NOW() where id=$1 and deleted_at is null`, req.Id)
	if err != nil {
		fmt.Println("error while deleting posts")
		return &ps.Empty{}, err
	}
	if err != nil {
		fmt.Println("error while deleting posts")
		return &ps.Empty{}, err
	}
	
	return &ps.Empty{}, nil
}
func (p *PostRepo) DeletePostUserID(req *ps.PostUserId) (*ps.Empty, error) {
	id := p.Db.QueryRow(`UPDATE posts SET deleted_at=NOW() where user_id=$1 and deleted_at is null returning id`, req.UserId)
	media := &ps.Media{}
	err := id.Scan(&media.PostId)
	if err != nil {
		return &ps.Empty{}, nil
	}
	err = p.DeleteMedia(&ps.PostId{Id: media.PostId})
	if err != nil {
		return &ps.Empty{}, nil
	}
	return &ps.Empty{}, nil
}

func (p *PostRepo) GetPostUserId(req *ps.PostUserId) (*ps.Posts, error) {
	// fmt.Print("Helloworlds")
	rows, err := p.Db.Query(`select id, name, description, user_id from posts where user_id = $1`, req.UserId)
	if err != nil {
		fmt.Println("error while scanning posts")
		return &ps.Posts{}, err
	}
	defer rows.Close()
	posts := &ps.Posts{}
	for rows.Next() {
		post := &ps.PostResp{}
		err := rows.Scan(&post.Id, &post.Name, &post.Description, &post.UserId)
		if err != nil {
			fmt.Println("error while gettining posts")
			return &ps.Posts{}, err
		}
		rowMedia, err := p.Db.Query(`select id, post_id, 
			name, link, type from medias
			where post_id = $1`, post.Id)
		if err != nil {
			return &ps.Posts{}, err
		}
		defer rowMedia.Close()

		for rowMedia.Next() {
			media := &ps.Media{}
			err := rowMedia.Scan(
				&media.Id, &media.PostId, &media.Name, &media.Link, &media.Type)
			if err != nil {
				return &ps.Posts{}, err
			}
			post.Medias = append(post.Medias, media)
		}
		posts.Posts = append(posts.Posts, post)
	}
	// fmt.Println(posts)
	return posts, nil
}
