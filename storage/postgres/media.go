package postgres

import (
	"fmt"
	ps "post-service/genproto/post"
)

// func (p *PostRepo) CreatMedia(media *ps.Media) (*ps.Media, error) {
// 	trx, err := p.Db.Begin()
// 	if err != nil {
// 		fmt.Println("error while inserting new customer by transaction")
// 		return &ps.Media{}, err
// 	}
// 	defer trx.Rollback()
// 	trx.Commit()
// 	return media, p.Db.QueryRow(`
// 			INSERT INTO medias(
// 				post_id,
// 				name,
// 				link,
// 				type
// 			)
// 			values($1, $2, $3, $4)
// 		`, media.PostId, media.Name, media.Link, media.Type).Err()
// }

func (p *PostRepo) DeleteMedia(postId *ps.PostId) error {
	return p.Db.QueryRow(`UPDATE medias SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, postId.Id).Err()
}

func (p *PostRepo) GetMediasByPostId(postId *ps.PostId) ([]*ps.Media, error) {
	rows, err := p.Db.Query(`SELECT 
	post_id, 
	name, 
	link, 
	type
	FROM medias where post_id=$1`, postId.Id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	response := []*ps.Media{}
	for rows.Next() {
		temp := &ps.Media{}
		err = rows.Scan(
			&temp.PostId,
			&temp.Name,
			&temp.Link,
			&temp.Type,
		)
		if err != nil {
			return nil, err
		}
		response = append(response, temp)
	}
	return response, nil
}

func (p *PostRepo) UpdateMediapostId(medias []*ps.Media) error {
	for _, m:= range medias{
		err := p.Db.QueryRow(`UPDATE medias
		SET name=$1, link=$2, type=$3, 
		updated_at=NOW() where post_id=$4`,
		m.Name, m.Link, m.Type, m.PostId).Err()
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	return nil
}