package repo

import (
	ps "post-service/genproto/post"
)

type PostStoreI interface {
	CreatePost(*ps.PostReq) (*ps.PostResp, error)
	UpdatePost(*ps.PostReq) (*ps.PostReq, error)
	DeletePostUserID(*ps.PostUserId) (*ps.Empty, error)
	DeletePostID(*ps.PostId) (*ps.Empty, error)
	GetPostUserId(*ps.PostUserId) (*ps.Posts, error)
	GetPostId(*ps.PostId) (*ps.PostResp, error)
}
